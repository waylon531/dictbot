# DIRBOT

DIRBOT is a matrix bot that users can use to store and retrieve defintions

Commands:
`dirbot: <a> is <b>`: This remembers that the definition of `a` is `b`
`dirbot: <a>`: This recalls the definition of `a`
`dirbot: forget <a>`: This forgets the definition of `a`

DIRBOT is configured via a `config.toml`. An example configuration file is
provided at `config.toml.sample`, and this should be edited and moved to
`config.toml` to configure the bot. After it's all configured you can just run
the bot with `cargo run`!
