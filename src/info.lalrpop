//
//    infobot, a matrix bot for storing information on things
//    Copyright (C) 2019 Waylon Cude
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, under version 3 of the License.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

use crate::command::Command;

grammar;

match {
    r"\w+\+\+",
    r"\w+\-\-",
    r"\w+[?.]",
    r"dictbot:" => NAME,
    r"(is|are)" => IS,
} else {
    _
}

pub Message: Vec<Command> = {
    NAME <l:Line> => vec![l],
}


Line: Command = {
    <n:Key> IS <k:Value> => Command::Set(n,k),
    "forget" <n:Key> => Command::Forget(n),
    <n:Key> => Command::Search(n),
};

EndWord: String = r"\w+[?.]" => <>.to_owned();

MinusWord: String = r"\w+\-\-" => {
    let mut s = <>.to_owned();
    s.pop();
    s.pop();
    s
};

PlusWord: String = r"\w+\+\+" => {
    let mut s = <>.to_owned();
    s.pop();
    s.pop();
    s
};

Word: String = {
    r"[^ ]+" => <>.to_owned(),
    //<EndWord>
};
MultiWord: String = {
    <s:Word*> <k:Word> => {
        let mut words = s.iter();
        let first = words.next().unwrap().to_string();
        let w = words.fold(first,|x,y| format!("{} {}",x,y));
        format!("{} {}",w,k)
    },
}

Key: String = {
    <s:KeyWord+> => {
        let mut words = s.iter();
        let first = words.next().unwrap().to_string();
        words.fold(first,|x,y| format!("{} {}",x,y))
    },
    <IS> => <>.to_owned(),
};

KeyWord: String = {
    //r"[^ ]+" => <>.to_owned(),
    <Word>,
    // ++ words are a different token
    r"\w+\+\+" => <>.to_owned(),
    NAME => <>.to_owned(),
};


Value: String = <s:ValueWord+> => {
    let mut words = s.iter();
    let first = words.next().unwrap().to_string();
    words.fold(first,|x,y| format!("{} {}",x,y))
};

ValueWord: String = {
    <InlineValueWord>,
    //<KeyWord>,
    <EndWord>,
    //"is" => "is".to_owned(),
    //"forget" => "forget".to_owned(),
};

// A subset of values that does not contain EndWords

InlineValue: String = <s:InlineValueWord+> => {
    let mut words = s.iter();
    let first = words.next().unwrap().to_string();
    words.fold(first,|x,y| format!("{} {}",x,y))
};

InlineValueWord: String = {
    <KeyWord>,
    <IS> => <>.to_owned(),
    "forget" => "forget".to_owned(),
};

